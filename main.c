#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <float.h>
#include <limits.h>
#include <pthread.h>
#include <curl/curl.h>

#define BUFSIZE 8192

#undef EXIT_FAILURE
#define EXIT_FAILURE 0 /* Lord forgive me */

typedef struct _thread_data
{
	double time_taken;
	char *url;
	long retcode;
	bool alive;
} thread_data;


double get_head_time(char *url, long *http_code)
{
	CURL *ch;
	CURLcode err;
	double time_taken;

	/*printf("Got %s.\n", url);*/

	ch = curl_easy_init();
	if (!ch)
		return -1;

	curl_easy_setopt(ch, CURLOPT_URL, url); /* Set the URL */
	curl_easy_setopt(ch, CURLOPT_NOBODY, 1L); /* Just the HEAD */


	if ((err = curl_easy_perform(ch)) != CURLE_OK) /* Punch it */
	{
		printf("Error: %s\n", curl_easy_strerror(err));
		return -2;
	}

	if (curl_easy_getinfo(ch, CURLINFO_TOTAL_TIME, &time_taken) != CURLE_OK) return -1;
	if (curl_easy_getinfo(ch, CURLINFO_RESPONSE_CODE, http_code) != CURLE_OK) return -1;

	curl_easy_cleanup(ch);

	return time_taken;

}


void *head_thread(void *arg)
{
	char *url;
	thread_data *vars = arg;
	url = vars->url;

	vars->time_taken = get_head_time(url, &vars->retcode);

	vars->alive = false;

	return NULL;
}


int main(int argc, char *argv[])
{
	int i, lowi;
	double lowt;
	char *urls;
	bool run;
	thread_data *data;
	pthread_t *threads;


	if (argc < 3)
	{
		fprintf(stderr, "Usage: %s [url1] [url2] [...]\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!(threads = malloc(sizeof (pthread_t) * argc))) return EXIT_FAILURE;
	if (!(data = malloc(sizeof (thread_data) * argc))) return EXIT_FAILURE;
	if (!(urls = malloc(BUFSIZE * argc))) return EXIT_FAILURE;

	for (i = 1; i < argc; i++)
	{
		strcpy(urls+(BUFSIZE*(i-1)), argv[i]); /* TODO: Use real syntax */
		data[i-1].url = urls+(BUFSIZE*(i-1));
		data[i-1].alive = true;
		data[i-1].time_taken = DBL_MAX;
		data[i-1].retcode = -1;
		pthread_create(&threads[i-1], NULL, head_thread, (void*)&data[i-1]);
	}


	run = true;
	while (run)
		for (i = 0; i < argc-1 && run; i++)
			if (!data[i].alive)
				run = false;

	lowt = DBL_MAX;
	lowi = 0;
	for (i = 0; i < argc-1; i++) /* Linear search for lowest time */
		if (data[i].time_taken < lowt)
			lowi = i;

	#ifdef _VERBOSE
	for (i = 0; i < argc-1; i++)
		fprintf(stderr, "Thread: %s - %f\n", data[i].url, data[i].time_taken);
	fprintf(stderr, "Fastest thread: %s - %f - %ld\n", data[lowi].url,
	        data[lowi].time_taken, data[lowi].retcode);
	#endif


	printf("%s", data[lowi].url);

	switch (data[lowi].retcode)
	{
		default: /* TODO: Have this be good */
		case 200:
		case 202:
			return 2;
		case 404:
		case 400:
		case 401:
			return 4;
		case 300:
		case 301:
		case 302:
		case 304:
			return 3;
		case 500:
		case 501:
		case 502:
		case 503:
		case 504:
			return 5;
	}
}
